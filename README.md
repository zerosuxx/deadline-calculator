# Deadline calculator

## Requirements
- Node 18 with NPM or Docker

### Setup application locally
```shell
$ npm install
```

### Run tests
```shell
$ npm test
# in watch mode
$ npm run test:watch
```

### Run app server
```shell
$ npm run serve

# call api endpoint with curl:
$ curl http://localhost:8080/calculate-deadline/2023-03-13T10:00/10
```

### Run tests with docker
```shell
$ docker-compose run --rm app npm t
# optionally if 'make' is installed
$ make test
```

### Run app server with docker
```shell
$ docker-compose up
# optionally if 'make' is installed
$ make up

# call api endpoint with curl:
$ curl http://localhost:8080/calculate-deadline/2023-03-13T10:00/10
```


## Task description

This exercise aims to assess your algorithmic skills and ability to write production code. The task involves creating a deadline calculator program that determines the resolution date and time for reported problems (bugs) based on specific rules:

- Working hours are from 9 AM to 5 PM, Monday through Friday.
- The program doesn't account for holidays; a holiday on a Thursday is still considered a working day, and a working Saturday is considered a nonworking day.
- Turnaround time is given in working hours. For instance, 2 days are equivalent to 16 working hours.
- Problems can only be reported during working hours (9 AM to 5 PM).
- Your main task is to implement the calculateDeadline method, which takes the submit date and turnaround time as input and returns the date and time when the issue should be resolved.
- Usage of third-party date-time libraries like moment.js or date-fns not allowed.

## Additional Information:

- Creating automated tests for your solution is beneficial but not mandatory. Test-driven development (TDD) solutions are appreciated.
- Writing clean code, as defined by Robert C. Martin, is encouraged.
- Using GIT is appreciated.
- Please integrate the implementation either within a user interface (UI) or behind an API endpoint.
- You can use your preferred programming language.
- Avoid using pseudo-code; instead, provide actual code that you would commit/push to a repository and that solves the given problem.
- Do NOT spend more time with the task than 24 hours.
- You have a maximum of 3 days to submit your solution.
- Even if you are not fully finished, you should submit your solution.
- If you have any further questions, feel free to ask.
