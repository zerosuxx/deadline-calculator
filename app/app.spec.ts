import App from './app';
import supertest, { SuperTest } from 'supertest';

describe('DeadlineCalculator Integration tests', () => {
    let requestWithSupertest: SuperTest<supertest.Test>;

    beforeAll(() => {
        const server = App.create()
            .configureRoutes()
            .getServer();
        requestWithSupertest = supertest(server);
    });

    it('GET /calculate-deadline endpoint with invalid submit date should returns success false with error message', async () => {
        const res = await requestWithSupertest.get('/calculate-deadline/2023-03-11T10:00/1');

        expect(res.status).toEqual(200);
        expect(res.type).toEqual('application/json');
        expect(res.body).toHaveProperty('success', false)
        expect(res.body).toHaveProperty('message', 'invalid submit date, the given date must be in week days')
    });

    it('GET /calculate-deadline endpoint with correct parameters should returns proper deadline', async () => {
        const res = await requestWithSupertest.get('/calculate-deadline/2023-03-13T10:00/1');

        expect(res.status).toEqual(200);
        expect(res.type).toEqual('application/json');
        expect(res.body).toHaveProperty('success', true)
        expect(res.body).toHaveProperty('message', '2023. 03. 13. 11:00:00')
    });
});
