import { Request, Response } from 'express';
import DeadlineCalculatorService from '../service/deadline-calculator';

export default class DeadlineCalculator {
    private readonly DATE_LOCALE = 'hu-HU';

    static create(): DeadlineCalculator {
        return new DeadlineCalculator(new DeadlineCalculatorService());
    }

    constructor(private calculator: DeadlineCalculatorService) {}

    handle(req: Request, res: Response): Response {
        try {
            const submitDate = new Date(Date.parse(req.params['submitDate']));
            const turnaroundTime = parseInt(req.params['turnaroundTime']);
            const dueDate = this.calculator.calculateDeadline(submitDate, turnaroundTime);

            return res.json({
                success: true,
                message: dueDate.toLocaleString(this.DATE_LOCALE),
            });
        } catch (e) {
            console.error(e);

            return res.json({
                success: false,
                message: (e as Error).message,
            });
        }
    }
}
