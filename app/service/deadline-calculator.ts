export default class DeadlineCalculator {
    private readonly SATURDAY_WEEK_NUMBER = 6;
    private readonly SUNDAY_WEEK_NUMBER = 0;
    private readonly WORK_HOUR_START = 9;
    private readonly WORK_HOUR_END = 17;

    calculateDeadline(submitDate: Date, turnaroundTime: number): Date {
        this.validateSubmitDate(submitDate);
        this.validateTurnaroundTime(turnaroundTime);

        return this.calculateDueDate(submitDate, turnaroundTime);
    }

    private calculateDueDate(submitDate: Date, turnaroundTime: number): Date {
        const dueDate = new Date(submitDate.getTime());

        while (turnaroundTime > 0) {
            dueDate.setHours(dueDate.getHours() + 1);
            if (this.isOutsideOfWorkHours(dueDate) || this.isInWeekend(dueDate)) {
                continue;
            }

            turnaroundTime--;
        }

        return dueDate;
    }

    private validateSubmitDate(date: Date): void {
        if (this.isInWeekend(date)) {
            throw new Error('invalid submit date, the given date must be in week days');
        }

        if (this.isOutsideOfWorkHours(date)) {
            throw new Error('invalid submit date, the given date must be in work hours');
        }
    }

    private validateTurnaroundTime(time: number): void {
        if (time < 0) {
            throw new Error('invalid turnaround time, the given value must be is greater than 0');
        }
    }

    private isInWeekend(date: Date): boolean {
        return date.getDay() === this.SATURDAY_WEEK_NUMBER || date.getDay() === this.SUNDAY_WEEK_NUMBER;
    }

    private isOutsideOfWorkHours(date: Date): boolean {
        return date.getHours() < this.WORK_HOUR_START || date.getHours() >= this.WORK_HOUR_END;
    }
}
