import 'jest';
import DeadlineCalculator from './deadline-calculator';

describe('Deadline Calculator', () => {
    const saturday = new Date('2023-03-11T10:00');
    const sunday = new Date('2023-03-12T10:00');
    const beforeWorkHours = new Date('2023-03-13T08:59');
    const afterWorkHours = new Date('2023-03-13T17:00');
    const validSubmitDate = new Date('2023-03-13T09:00');

    let calculator: DeadlineCalculator;

    beforeEach(() => {
        calculator = new DeadlineCalculator();
    });

    it.each([
        saturday,
        sunday
    ])('throws exception when the submitted date in weekend', (submitDate) => {
        expect(() => calculator.calculateDeadline(submitDate, 0))
            .toThrow('invalid submit date, the given date must be in week days');
    });

    it.each([
        beforeWorkHours,
        afterWorkHours]
    )('throws exception when the submitted date is outside of working hours', (submitDate) => {
        expect(() => calculator.calculateDeadline(submitDate, 0))
            .toThrow('invalid submit date, the given date must be in work hours');
    });

    it('throws exception when the turnaround time is negative', () => {
        expect(() => calculator.calculateDeadline(validSubmitDate, -1))
            .toThrow('invalid turnaround time, the given value must be is greater than 0');
    });

    it('returns same date when turnaround time is zero', () => {
        expect(calculator.calculateDeadline(validSubmitDate, 0)).toStrictEqual(validSubmitDate);
    });

    it('returns simple resolved task on the same day', () => {
        const submitDate = new Date('2023-03-13T09:00');
        const turnaroundTime = 1;
        const expectedDate = new Date('2023-03-13T10:00');

        expect(calculator.calculateDeadline(submitDate, turnaroundTime)).toStrictEqual(expectedDate);
    });

    it('given submit date is not mutated', () => {
        const submitDate = new Date('2023-03-13T09:00');
        const expectedSubmitDate = new Date(submitDate.getTime());

        calculator.calculateDeadline(submitDate, 1);

        expect(submitDate).toStrictEqual(expectedSubmitDate);
    });

    it('returns resolved task on another day', () => {
        const submitDate = new Date('2023-03-13T16:59');
        const turnaroundTime = 1;
        const expectedDate = new Date('2023-03-14T09:59');

        expect(calculator.calculateDeadline(submitDate, turnaroundTime)).toStrictEqual(expectedDate);
    });

    it('returns resolved task more than 2 days later', () => {
        const submitDate = new Date('2023-03-13T10:00');
        const turnaroundTime = 10;
        const expectedDate = new Date('2023-03-14T12:00');

        expect(calculator.calculateDeadline(submitDate, turnaroundTime)).toStrictEqual(expectedDate);
    });

    it('returns resolved task in first work day after weekend', () => {
        const submitDate = new Date('2023-03-10T10:00');
        const turnaroundTime = 8;
        const expectedDate = new Date('2023-03-13T10:00');

        expect(calculator.calculateDeadline(submitDate, turnaroundTime)).toStrictEqual(expectedDate);
    });

    it('returns resolved task weeks later', () => {
        const submitDate = new Date('2023-03-10T10:00');
        const turnaroundTime = 72;
        const expectedDate = new Date('2023-03-23T10:00');

        expect(calculator.calculateDeadline(submitDate, turnaroundTime)).toStrictEqual(expectedDate);
    });
});
