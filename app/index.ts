import App from './app';

const PORT = parseInt(process.env.PORT|| '8080');

((app) => {
    app.configureRoutes()
        .getServer()
        .listen(PORT, () => console.log(`Server listening on: http://localhost:${PORT}`));
})(App.create());
