import express, { Express } from 'express';
import routes from './config/routes';

export default class App {
    static create(): App {
        return new App(express());
    }

    constructor(private server: Express) {}

    getServer(): Express {
        return this.server;
    }

    configureRoutes() {
        this.server.use(routes);

        return this;
    }
}
