import { Request, Response, Router } from 'express';
import DeadlineCalculator from '../action/deadline-calculator';

const router = Router();

router.get('/calculate-deadline/:submitDate/:turnaroundTime', (req: Request, res: Response) =>
    DeadlineCalculator.create().handle(req, res));

export default router;
